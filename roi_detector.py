from pretrain_filters import *


class ROI_detector:

    def load_filters(self, filters_filename):
        self.w_conv1 = np.load(filters_filename)['data']['W_1']
        self.b_conv1 = np.load(filters_filename)['data']['b_1']

        self.filter_height = self.w_conv1.shape[0]
        self.filter_width = self.w_conv1.shape[1]
        self.filter_num = self.w_conv1.shape[3]

    def train_filters(self, _lambda, _beta, _p):
        self.w_conv1, self.b_conv1 = pretrain_filters(
            '/home/intern/data/2016_second_annual_data_science_bowl/Sunnybrook_data/challenge_training/',
            (self.filter_height, self.filter_width), self.filter_num, self.hidden_layer_size, _lambda, _beta, _p)

    def __init__(self, filter_size, filter_num, hidden_layer_size, _lambda, _beta, _p):
        assert len(filter_size) == 2

        self.hidden_layer_size = hidden_layer_size
        self.filter_height = filter_size[0]
        self.filter_width = filter_size[1]
        self.filter_num = filter_num

        self.w_conv1 = tf.Variable(self.w_conv1, name="W_conv1")
        self.b_conv1 = tf.Variable(self.b_conv1, name="b_conv1")


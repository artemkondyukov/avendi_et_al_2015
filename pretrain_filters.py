import dicom
import fnmatch
import numpy as np
import os

import tensorflow as tf
from initializer import weight_variable, bias_variable
from autoencoder import Autoencoder


def obtain_filters(images_path, filter_size, filter_num):
    # Opens images_path, gets all DICOM images from there and takes random patches from random images
    assert 0 < len(filter_size) < 3

    filter_w = filter_size if len(filter_size) == 1 else filter_size[1]
    filter_h = filter_size if len(filter_size) == 1 else filter_size[0]

    dcm_files = [os.path.join(dirpath, f)
                 for dirpath, _, files in os.walk(images_path)
                 for f in fnmatch.filter(files, '*.dcm')]

    np.random.shuffle(dcm_files)

    filter_counter = 0
    last_filter = 0
    filters = np.empty((filter_num, filter_size[0], filter_size[1]))

    for i in range(len(dcm_files)):
        if filter_counter >= filter_num:
            break

        # number of filters we are to get from the current image
        # 5 here is to address rounding and exceptional cases
        curr_filter_num = int(np.round(np.random.exponential(5 * float(filter_num) / len(dcm_files))))

        if curr_filter_num <= 0:
            continue

        filter_counter += curr_filter_num

        # in case of some problems with an image
        try:
            image = dicom.read_file(dcm_files[i]).pixel_array
        except ValueError:
            filter_counter -= curr_filter_num
            continue

        for j in range(curr_filter_num):
            assert len(image.shape) < 4

            if len(image.shape) > 2:
                image = image.mean(0)

            h, w = image.shape
            h -= filter_h - 1
            w -= filter_w - 1

            y = np.random.randint(0, h)
            x = np.random.randint(0, w)
            filters[last_filter] = np.copy(image[y:y + filter_h, x:x + filter_w])
            last_filter += 1

            if last_filter >= filter_num:
                break

    # normalize data
    return filters / filters.max()


def pretrain_filters(images_path, filter_size, filter_num, hidden_layer_size, _lambda, _beta, _p):
    # constructs autoencoder, trains it and returns weights and biases of input-hidden layer transition

    # _lambda is a regularization coefficient
    # _beta is Kullback-Leibler coefficient
    # _p is Kullback-Leibler parameter

    assert 0 < len(filter_size) < 3

    filters = obtain_filters(images_path, filter_size, filter_num)
    filters = np.array([f.flatten() for f in filters])

    with Autoencoder(filters, hidden_layer_size, _lambda, _beta, _p) as autoenc:
        autoenc.train(filters, 100000)

        w_1 = autoenc.get_w_1()
        b_1 = autoenc.get_b_1()

    w_1_r = np.reshape(w_1, [filter_size[0], filter_size[1], 1, hidden_layer_size])

    return w_1_r, b_1


if __name__ == "__main__":
    # for l in [1e-7, 3e-7, 1e-6, 3e-6, 1e-5, 3e-5, 1e-4, 3e-4, 1e-3]:
    for l in [1e-5, 3e-5, 1e-4, 3e-4, 1e-3]:
        for b in [1e-4, 3e-4, 1e-3, 3e-3, 1e-2, 3e-2]:
            if os.path.exists("./filters_" + str(l) + "_" + str(b)):
                continue
            W_1, b_1 = pretrain_filters(
                '/home/intern/data/2016_second_annual_data_science_bowl/Sunnybrook_data/challenge_training/',
                (11, 11), 1000, 100, 1e-6, 3e-3, 0.1)

            with open("./filters_" + str(l) + "_" + str(b), "w") as f:
                np.savez_compressed(f, data={'W_1': W_1, 'b_1': b_1})
    #
    # W_1, b_1 = pretrain_filters(
    #     '/home/intern/data/2016_second_annual_data_science_bowl/Sunnybrook_data/challenge_training/',
    #     (11, 11), 1000, 100, 1e-7, 3e-4, 0.1)

    print "Filters were trained"

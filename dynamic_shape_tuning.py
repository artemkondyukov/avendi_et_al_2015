import fnmatch
import itertools
import numpy as np
import operator
import os
from scipy.ndimage import gaussian_filter
from scipy.misc import imresize

from stacked_ae import predict

THRESHOLD = 0.5
LEARNING_RATE = 1e-6
DOWNSAMPLED_IMAGE_SIZE = 64
MIN_DIFF = 0.1


def reg_heaviside(x, eps):
    # Regularized Heaviside step function
    return (1 + 2 / np.pi * np.arctan(x / eps)) / 2


def reg_delta(x, eps):
    # Regularized Dirac function
    return eps / (np.pi * (x ** 2 + eps ** 2))


def convert_mask_to_contour(mask):
    """
    Converts a binary mask to contours
    :param mask: a 2d array, where 1s is 'inside', 0s is 'outside'
    :return: result: a 2d array, where 1s is contour
    """

    mask = np.copy(mask).astype(np.int8)
    assert mask.max() <= 1 and mask.min() >= 0

    result = np.zeros_like(mask, dtype=np.bool)
    positions = np.where(mask == 1)
    for position in zip(positions[0], positions[1]):
        prod = [map(operator.add, position, (x, y)) for x, y in itertools.product([-1, 0, 1], [-1, 0, 1])]
        xs = np.array(prod)[..., 1]
        ys = np.array(prod)[..., 0]
        if 0 in mask[ys, xs]:
            result[position] = True

    return result


def convert_mask_to_phi(mask):
    """
    Converts a mask into a Lipschitz-function which is positive inside the contour and negative outsize.
    Also we introduce convolution of the mask so as not to lose the actual contour
    :param mask: a 2d array where intensities represent certainty about pixel belonging to the object [0..1]
    :return: a 2d array representing an image of \phi(x, y)
    """
    result = np.copy(mask)
    result -= THRESHOLD
    result = gaussian_filter(result, 0.5)
    return result


def E_len(phi, image):
    """
    Computes length-based energy function E_len(\phi) = \Integrate_{\Omega_S} \delta(\phi) |\nabla \phi| dxdy
    \delta(\phi) is the derivative of Heaviside step function
    :param phi: a 2d array representing values of \phi
    :param image: a 2d array representing the input image
    :return: result: float, E_len term of the overall energy function
    """
    assert len(phi.shape) == 2
    assert phi.shape == image.shape

    # DEPRECATED (w/o regularization)
    # binary_mask = np.cast(mask > THRESHOLD, np.int8)
    # contour = convert_mask_to_contour(binary_mask)
    # gradient = np.gradient(image)
    # grad_amplitude = np.sqrt(np.square(gradient[0]) + np.square(gradient[1]) ** 2)
    # result = np.sum(grad_amplitude * contour)

    eps = 0.003
    delta = reg_delta(phi, eps)
    gradient = np.gradient(phi)
    grad_amplitude = np.sqrt(np.square(gradient[0]) + np.square(gradient[1]))
    grad_amplitude[grad_amplitude == 0] = 1
    result = np.sum(grad_amplitude * delta)

    derivative = delta * reduce(np.add, gradient / grad_amplitude)

    return result, derivative


def E_reg(phi, image):
    """
    Computes region-based energy function:
    E_reg(\phi) =
    = \Integrate_{\Omega_S} (I_s - c_1)^2 H(\phi) dxdy +
    + \Integrate_{\Omega_S} (I_s - c_2)^2 (1 - H(\phi)) dxdy
    c_1 is the average across the exterior
    c_2 is the average across the interior
    H(\phi) is the Heaviside step function (probably regularized in some way)
    I_s is the image to be processed
    :param phi: a 2d array representing values of \phi
    :param image: a 2d array representing the input image
    :return: result: float, E_reg term of the overall energy function
    """
    assert len(phi.shape) == 2
    assert phi.shape == image.shape

    # DEPRECATED w/o regularization
    # binary_mask = np.cast(mask >= THRESHOLD, np.int8)
    # contour = np.cast(convert_mask_to_contour(binary_mask), np.int8)
    # interior = np.subtract(np.cast(mask >= THRESHOLD, np.int8), contour)
    # exterior = np.cast(mask < THRESHOLD + contour / 2, np.int8)
    # c_1 = np.mean(image * exterior)
    # c_2 = np.mean(image * interior)
    # result = np.sum(np.square(image - c_1) * exterior + np.square(image - c_2) * interior)

    eps = 0.003
    delta = reg_delta(phi, eps)
    delta_rect = np.copy(delta)
    delta_rect[delta >= 2] = 2
    delta_rect[delta < 0.2] = 0
    exterior = phi <= 0
    interior = phi > 0
    c_1 = np.mean(image[np.where(exterior == 1)])
    c_2 = np.mean(image [np.where(interior == 1)])
    result = np.sum(np.square(image - c_1) * exterior + np.square(image - c_2) * interior)

    derivative = delta_rect * (((image - c_1) ** 2) - ((image - c_2) ** 2))

    return result, derivative


def E_shape(phi, tuned):
    """
    Computes region-based energy function:
    E_shape (\phi) = \Integrate_{\Omega_S} (\phi - \phi_inferred)^2 dxdy
    :param phi: a 2d array representing the starting values of \phi
    :param tuned: a 2d array representing \phi function after several gradient descent iterations
    :return: result: float, E_shape term of the overall energy function
    """
    assert len(phi.shape) == 2
    assert phi.shape == tuned.shape
    result = np.sum(np.square(phi - tuned))
    # By now derivative is just zeros, it's reasonable. Trust me.
    derivative = np.zeros_like(phi)
    return result, derivative


if __name__ == '__main__':
    r_files = [os.path.join(dir_path, f)
               for dir_path, _, files in os.walk('./')
               for f in fnmatch.filter(files, 'rois[0-9]*.npz')]

    r_l_files = [os.path.join(dir_path, f)
               for dir_path, _, files in os.walk('./')
               for f in fnmatch.filter(files, 'rois_labels[0-9]*.npz')]

    ra = np.load(r_files[0])['data']
    ra = np.array([imresize(img, [DOWNSAMPLED_IMAGE_SIZE, DOWNSAMPLED_IMAGE_SIZE]) for img in ra], dtype=np.float32)
    r_la = np.load(r_l_files[0])['data']
    r_la = np.array([imresize(img, [DOWNSAMPLED_IMAGE_SIZE, DOWNSAMPLED_IMAGE_SIZE]) for img in r_la], dtype=np.float32)

    pa = predict(ra, 200)
    pa = np.array([np.reshape(a, [DOWNSAMPLED_IMAGE_SIZE, DOWNSAMPLED_IMAGE_SIZE]) for a in pa])

    for p, r, r_l in zip(pa, ra, r_la):
        r_l = r_l / r_l.max()
        r = r / r.max()
        tuned_shape = convert_mask_to_phi(np.copy(p))
        prev_tuned = np.zeros_like(tuned_shape)
        p = convert_mask_to_phi(p)

        for i in range(100000):
            # IT'S DELIRIUM. WE NEED PROCESS IMAGES ONE-BY-ONE!
            # len_energy_arr = [E_len(pe, re) for pe, re in zip(p, r)]
            # len_energy = np.array([tup[0] for tup in len_energy_arr]).sum()
            # len_energy_der = np.array([tup[1] for tup in len_energy_arr]).sum(0)
            #
            # reg_energy_arr = [E_reg(pe, re) for pe, re in zip(p, r)]
            # reg_energy = np.array([tup[0] for tup in reg_energy_arr]).sum()
            # reg_energy_der = np.array([tup[1] for tup in reg_energy_arr]).sum(0)
            #
            # shape_energy_arr = [E_shape(pe, tse) for pe, tse in zip(p, tuned_shape)]
            # shape_energy = np.array([tup[0] for tup in shape_energy_arr]).sum()
            # shape_energy_der = np.array([tup[1] for tup in shape_energy_arr]).sum(0)

            len_energy = E_len(tuned_shape, r)
            len_energy_der = len_energy[1]
            len_energy = len_energy[0]

            reg_energy = E_reg(tuned_shape, r)
            reg_energy_der = reg_energy[1] / 4
            reg_energy = reg_energy[0] / 4

            shape_energy = E_shape(p, tuned_shape)
            shape_energy_der = shape_energy[1]
            shape_energy = shape_energy[0]

            # if i % 30000 == 0:
            #     print "ABC"

            if i % 10000 == 0:
                print "Energy: ", len_energy + reg_energy + shape_energy

            tuned_shape += LEARNING_RATE * (len_energy_der + reg_energy_der + shape_energy_der)

            if np.sum(np.square(prev_tuned - tuned_shape)) < MIN_DIFF:
                break

        p = np.array(p > 0, dtype=np.int8)
        tuned_shape = np.array(tuned_shape > 0, dtype=np.int8)
        print "Start: ", np.mean(np.square(p - r_l)), "Finally: ", np.mean(np.square(tuned_shape - r_l))

    print "123"

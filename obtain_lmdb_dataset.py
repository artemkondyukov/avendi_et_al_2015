import lmdb
import caffe
import numpy as np
import caffe.proto.caffe_pb2


IMAGES_TO_PROCESS = -1


def load(images_lmdb_path, labels_lmdb_path):

    images = np.array([[[]]])
    labels = np.array([[[]]])

    images_env = lmdb.open(images_lmdb_path)
    images_txn = images_env.begin()
    images_cursor = images_txn.cursor()

    labels_env = lmdb.open(labels_lmdb_path)
    labels_txn = labels_env.begin()
    labels_cursor = labels_txn.cursor()

    datum = caffe.proto.caffe_pb2.Datum()

    counter = 0

    for key, value in images_cursor:
        datum.ParseFromString(value)
        tmp = caffe.io.datum_to_array(datum)
        if images.size == 0:
            images = np.empty_like(tmp)

        images = np.append(images, tmp, axis=0)
        # if images.shape[0] > 9:
        #     break

        counter += 1
        if IMAGES_TO_PROCESS != -1 and counter >= IMAGES_TO_PROCESS:
            break

        print key

    counter = 0

    for key, value in labels_cursor:
        datum.ParseFromString(value)
        tmp = caffe.io.datum_to_array(datum)
        if labels.size == 0:
            labels = np.empty_like(tmp)
        labels = np.append(labels, tmp, axis=0)
        # if labels.shape[0] > 9:
        #     break

        counter += 1
        if IMAGES_TO_PROCESS != -1 and counter >= IMAGES_TO_PROCESS:
            break

        print key

    return images[1:], labels[1:]

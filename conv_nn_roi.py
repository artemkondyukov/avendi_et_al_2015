from scipy.misc import imresize
from scipy import ndimage
from matplotlib import pyplot as plt

import datetime

from pretrain_filters import *
import obtain_lmdb_dataset

LAMBDA = 0.000001
BETA = 0.03
P = 0.1
IMAGE_SIZE = 256
DOWNSAMPLED_IMAGE_SIZE = 64
MASK_IMAGE_SIZE = 32
BATCH_SIZE = 20
TEST_SET_SIZE = 0.2  # 20% of all images


def conv2d(x, W):
    return tf.nn.conv2d(x, W, strides=[1, 1, 1, 1], padding='VALID')


def avg_pool_6x6(x):
    return tf.nn.avg_pool(x, ksize=[1, 6, 6, 1], strides=[1, 6, 6, 1], padding='SAME')


def preprocess_images_and_labels(images, labels, size):
    assert len(images) == len(labels)
    assert 0 < len(size) < 3
    final_w = size[1] if len(size) == 2 else size[0]
    final_h = size[0]

    result_images = np.empty((len(images), final_h, final_w))
    result_labels = np.empty((len(labels), final_h, final_w))
    img_counter = 0

    for img, label in zip(images, labels):
        assert len(img.shape) == 2
        assert img.shape == label.shape

        # TODO implement ratio-preserving resize with fulfilling margins with zeros
        # h, w = img.shape
        # ratio_h = float(final_h) / h
        # ratio_w = float(final_w) / w
        #
        # ratio = ratio_h if np.abs(np.log(ratio_h)) < np.abs(np.log(ratio_w)) else ratio_w

        tmp_image = imresize(img, (final_h, final_w))
        tmp_label = imresize(label, (final_h, final_w))

        result_images[img_counter] = np.copy(tmp_image)
        result_labels[img_counter] = np.copy(tmp_label)

        img_counter += 1

    return result_images, result_labels


def train_nn_roi(filter_num, filter_size, hidden_layer_size):
    assert 0 < len(filter_size) < 3

    filter_w = filter_size if len(filter_size) == 1 else filter_size[1]
    filter_h = filter_size if len(filter_size) == 1 else filter_size[0]

    images, labels = obtain_lmdb_dataset.load('./train_images_lmdb', './train_labels_lmdb')
    images_s, labels_s = preprocess_images_and_labels(images, labels, (MASK_IMAGE_SIZE, MASK_IMAGE_SIZE))
    images, labels = preprocess_images_and_labels(images, labels, (DOWNSAMPLED_IMAGE_SIZE, DOWNSAMPLED_IMAGE_SIZE))

    # Instead of actual labelled data we need generated ROIs,
    # which are actually rectangles 100x100 (from 256x256 image) centered to center of contour

    # images = tf.reshape(images, shape=[-1, np.square(DOWNSAMPLED_IMAGE_SIZE)])
    # labels = tf.reshape(labels, shape=[-1, np.square(DOWNSAMPLED_IMAGE_SIZE)])

    images = images.reshape([images.shape[0], np.square(DOWNSAMPLED_IMAGE_SIZE)])
    labels_s = labels_s.reshape([images_s.shape[0], np.square(MASK_IMAGE_SIZE)]).astype(np.float32)
    # labels = labels.reshape([labels.shape[0], np.square(DOWNSAMPLED_IMAGE_SIZE)])

    W_conv1, b_conv1 = pretrain_filters(
        '/home/intern/data/2016_second_annual_data_science_bowl/Sunnybrook_data/challenge_training/',
        filter_size, filter_num, hidden_layer_size, LAMBDA, BETA, P)
    W_conv1 = tf.Variable(W_conv1, name="W_conv1")
    b_conv1 = tf.Variable(b_conv1, name="b_conv1")

    # W_conv1 = weight_variable([11, 11, 1, 100])
    # b_conv1 = bias_variable([100])

    x = tf.placeholder(tf.float32, shape=[None, np.square(DOWNSAMPLED_IMAGE_SIZE)])
    y_ = tf.placeholder(tf.float32, shape=[None, np.square(MASK_IMAGE_SIZE)])

    x_image = tf.reshape(x, [-1, DOWNSAMPLED_IMAGE_SIZE, DOWNSAMPLED_IMAGE_SIZE, 1])
    h_conv1 = tf.nn.sigmoid(conv2d(x_image, W_conv1) + b_conv1)

    assert h_conv1.get_shape()[1] == DOWNSAMPLED_IMAGE_SIZE - 2 * (int(filter_h) / 2)
    assert h_conv1.get_shape()[2] == DOWNSAMPLED_IMAGE_SIZE - 2 * (int(filter_w) / 2)

    h_pool1 = avg_pool_6x6(h_conv1)

    pooled_w = int(h_conv1.get_shape()[2]) / 6
    pooled_h = int(h_conv1.get_shape()[1]) / 6
    assert h_pool1.get_shape()[1] == pooled_h
    assert h_pool1.get_shape()[2] == pooled_w

    W_fc1 = weight_variable([pooled_h * pooled_w * hidden_layer_size, np.square(MASK_IMAGE_SIZE)], "W_fc1")
    b_fc1 = bias_variable([np.square(MASK_IMAGE_SIZE)], "b_fc1")

    h_pool1_flat = tf.reshape(h_pool1, [-1, pooled_w * pooled_h * hidden_layer_size])
    h_fc1 = tf.sigmoid(tf.matmul(h_pool1_flat, W_fc1) + b_fc1)

    cross_entropy = tf.reduce_mean(tf.square(h_fc1 - y_)) + \
                    LAMBDA / 2 * (tf.reduce_sum(tf.square(W_conv1)) + tf.reduce_sum(tf.square(W_fc1)))

    h_fc1_r = tf.reshape(h_fc1, [-1, MASK_IMAGE_SIZE, MASK_IMAGE_SIZE])

    # Calculate dist accuracy
    # Find coordinates of mass center
    # coord_x = tf.cast(tf.arg_max(tf.reduce_mean(h_fc1_r, 1), 1), tf.float32)
    # coord_y = tf.cast(tf.arg_max(tf.reduce_mean(h_fc1_r, 2), 1), tf.float32)
    #
    # x_r = tf.reshape(x, [-1, MASK_IMAGE_SIZE, MASK_IMAGE_SIZE])
    # coord_x_ref = tf.cast(tf.arg_max(tf.reduce_mean(x_r, 1), 1), tf.float32)
    # coord_y_ref = tf.cast(tf.arg_max(tf.reduce_mean(x_r, 2), 1), tf.float32)
    # cross_entropy = tf.reduce_mean(tf.sqrt(tf.square(coord_x - coord_x_ref) + tf.square(coord_y - coord_y_ref)))

    train_step = tf.train.AdamOptimizer(1e-4).minimize(cross_entropy)
    init = tf.initialize_all_variables()

    # Add ops to save and restore all the variables.
    saver = tf.train.Saver()

    # config = tf.ConfigProto()
    # config.gpu_options.allocator_type = 'BFC'
    # session = tf.Session(config=config)
    session = tf.InteractiveSession()
    session.run(init)

    shuffled_indices = range(0, images.shape[0])
    np.random.shuffle(shuffled_indices)
    images = images[shuffled_indices]
    labels_s = labels_s[shuffled_indices]

    images_test = images[1:TEST_SET_SIZE * images.shape[0]]
    labels_s_test = labels_s[1:TEST_SET_SIZE * images.shape[0]]

    images = images[TEST_SET_SIZE * images.shape[0]:]
    labels_s = labels_s[TEST_SET_SIZE * images.shape[0]:]

    size_coeff = DOWNSAMPLED_IMAGE_SIZE / MASK_IMAGE_SIZE

    for i in range(1000):
        indices = np.random.randint(0, len(images), BATCH_SIZE)
        batch_x = images[indices]
        batch_y = labels_s[indices]

        train_step.run(feed_dict={x: batch_x, y_: batch_y})

        if i % 100 == 0:
            output = h_fc1.eval(feed_dict={x: batch_x, y_: batch_y})

            output = np.reshape(output, [output.shape[0], MASK_IMAGE_SIZE, MASK_IMAGE_SIZE])
            result = np.array([imresize(output[i], (64, 64)) for i in range(output.shape[0])], dtype=np.float32)
            result /= 255
            result = np.reshape(result, (result.shape[0], 64 * 64))

            # Calculate dist accuracy
            # Find coordinates of mass center
            coords = [ndimage.measurements.center_of_mass(c >= 0.5) for c in output]
            coord_x = [c[1] for c in coords]
            coord_y = [c[0] for c in coords]
            coord_x = np.round(size_coeff * coord_x).astype(np.int32)
            coord_y = np.round(size_coeff * coord_y).astype(np.int32)
            print coords

            input_ref = batch_y.reshape([len(batch_y), MASK_IMAGE_SIZE, MASK_IMAGE_SIZE])
            coords_ref = [ndimage.measurements.center_of_mass(c) for c in input_ref]
            coord_x_ref = [c[1] for c in coords_ref]
            coord_y_ref = [c[0] for c in coords_ref]
            coord_x_ref = np.round(size_coeff * coord_x_ref).astype(np.int32)
            coord_y_ref = np.round(size_coeff * coord_y_ref).astype(np.int32)
            print coords_ref

            print "Cross entropy: ", cross_entropy.eval(feed_dict={x: batch_x, y_: batch_y})
            print "Dist accuracy: ", \
                tf.reduce_mean(np.sqrt((coord_x - coord_x_ref) ** 2 + (coord_y - coord_y_ref) ** 2)). \
                    eval(feed_dict={x: batch_x, y_: batch_y})

            # print "Accuracy: ", np.array(((result >= 0.5) == (labels == 1))).mean()
            # accuracy = tf.reduce_mean(tf.cast(tf.equal(output, labels_s), tf.float32))
            # print "Accuracy: ", accuracy.eval(feed_dict={x: images, y_: labels_s})

    # Save the variables to disk.
    save_path = saver.save(session, "./model" + datetime.datetime.now().strftime("%Y-%m-%d %H:%M") + ".ckpt")
    print("Model saved in file: %s" % save_path)

    output = h_fc1.eval(feed_dict={x: images_test, y_: labels_s_test})
    output = np.reshape(output, [output.shape[0], MASK_IMAGE_SIZE, MASK_IMAGE_SIZE])
    # Calculate dist accuracy
    # Find coordinates of mass center
    coords = [ndimage.measurements.center_of_mass(c >= 0.5) for c in output]
    coord_x = [c[1] for c in coords]
    coord_y = [c[0] for c in coords]
    coord_x = np.round(size_coeff * coord_x).astype(np.int32)
    coord_y = np.round(size_coeff * coord_y).astype(np.int32)
    coord = np.array([coord_x, coord_y])

    input_ref = labels_s_test.reshape([len(labels_s_test), MASK_IMAGE_SIZE, MASK_IMAGE_SIZE])
    coords_ref = [ndimage.measurements.center_of_mass(c) for c in input_ref]
    coord_x_ref = [c[1] for c in coords_ref]
    coord_y_ref = [c[0] for c in coords_ref]
    coord_x_ref = np.round(size_coeff * coord_x_ref).astype(np.int32)
    coord_y_ref = np.round(size_coeff * coord_y_ref).astype(np.int32)
    coord_ref = np.array([coord_x_ref, coord_y_ref])

    print "Dist accuracy: ", \
        tf.reduce_mean([np.linalg.norm([x1 - x2, y1 - y2])
                        for x1, x2, y1, y2 in zip(coord_x, coord_x_ref, coord_y, coord_y_ref)]).\
            eval(feed_dict={x: images_test, y_: labels_s_test})

    for i in range(10):
        index = np.random.randint(0, images_test.shape[0])
        output = h_fc1.eval(feed_dict={x: np.array([images_test[index]]), y_: np.array([labels_s_test[index]])})
        output = np.reshape(output, [1, MASK_IMAGE_SIZE, MASK_IMAGE_SIZE])[0]
        # Find coordinates of mass center
        # coord_x = (DOWNSAMPLED_IMAGE_SIZE / MASK_IMAGE_SIZE) * np.argmax(output.mean(axis=1), axis=1)
        # coord_y = (DOWNSAMPLED_IMAGE_SIZE / MASK_IMAGE_SIZE) * np.argmax(output.mean(axis=2), axis=1)
        coord_y, coord_x = ndimage.measurements.center_of_mass(output >= 0.5)
        coord_x = np.round(size_coeff * coord_x).astype(np.int32)
        coord_y = np.round(size_coeff * coord_y).astype(np.int32)

        input_ref = labels_s_test[index].reshape([1, MASK_IMAGE_SIZE, MASK_IMAGE_SIZE])[0]
        # coord_x_ref = (DOWNSAMPLED_IMAGE_SIZE / MASK_IMAGE_SIZE) * \
        #     np.round(np.argwhere(input_ref.mean(axis=1)[0] != 0).mean())
        # coord_y_ref = (DOWNSAMPLED_IMAGE_SIZE / MASK_IMAGE_SIZE) * \
        #     np.round(np.argwhere(input_ref.mean(axis=2)[0] != 0).mean())
        coord_y_ref, coord_x_ref = ndimage.measurements.center_of_mass(input_ref)
        coord_x_ref = np.round(size_coeff * coord_x_ref).astype(np.int32)
        coord_y_ref = np.round(size_coeff * coord_y_ref).astype(np.int32)

        # tmp_image = imresize(output[0], (DOWNSAMPLED_IMAGE_SIZE, DOWNSAMPLED_IMAGE_SIZE))
        plt.figure(i)
        plt.imshow(np.reshape(images_test[index], [DOWNSAMPLED_IMAGE_SIZE, DOWNSAMPLED_IMAGE_SIZE]))
        plt.scatter(coord_x, coord_y, c='r', marker='x')
        plt.scatter(coord_x_ref, coord_y_ref, c='g', marker='o')

        plt.figure(i*1001)
        plt.imshow(imresize(np.reshape(output, [MASK_IMAGE_SIZE, MASK_IMAGE_SIZE]),
                            [DOWNSAMPLED_IMAGE_SIZE, DOWNSAMPLED_IMAGE_SIZE]))
        plt.scatter(coord_x, coord_y, c='r', marker='x')
        plt.scatter(coord_x_ref, coord_y_ref, c='g', marker='o')

    plt.show(block=True)
    return 0


if __name__ == "__main__":
    train_nn_roi(filter_num=1000, filter_size=(11, 11), hidden_layer_size=100)

from initializer import weight_variable, bias_variable

import numpy as np
import tensorflow as tf

BATCH_SIZE = 1000  # Actually it's just a constant, but it should fit into available memory


class Autoencoder:
    # It's an implementation of autoencoder with one hidden layer
    # Possibly it'll be improved to work with several hidden layers

    session_started = False

    def __init__(self, data, hidden_layer_size, _lambda=0.000001, _beta=0.03, _p=0.1):
        assert len(data.shape) == 2
        self.data = data
        self.hidden_layer_size = hidden_layer_size
        self._lambda = _lambda
        self._beta = _beta
        self._p = _p

        self.input_layer_size = data.shape[1]
        self.x = tf.constant(data, dtype=tf.float32)

        self.w_1 = weight_variable([self.input_layer_size, self.hidden_layer_size], "W_1")
        self.b_1 = bias_variable([self.hidden_layer_size], "b_1")

        self.w_2 = weight_variable([self.hidden_layer_size, self.input_layer_size], "W_2")
        self.b_2 = bias_variable([self.input_layer_size], "b_2")

        self.a_2 = tf.nn.sigmoid(tf.matmul(self.x, self.w_1) + self.b_1)
        self.y = tf.nn.sigmoid(tf.matmul(self.a_2, self.w_2) + self.b_2)

        self.p_cap = tf.reduce_mean(self.a_2, 0)
        self.cross_entropy = tf.reduce_mean(tf.square(self.x - self.y)) + \
                             self._lambda / 2 * (
                                 tf.reduce_sum(tf.square(self.w_1)) + tf.reduce_sum(tf.square(self.w_2))) + \
                             self._beta * tf.reduce_sum(
                                 _p * tf.log(_p / self.p_cap) + (1 - _p) * tf.log((1 - _p) / (1 - self.p_cap)))

        self.config = tf.ConfigProto()
        self.config.gpu_options.allocator_type = 'BFC'
        self.session = tf.Session(config=self.config)
        self.train_step = tf.train.AdamOptimizer(0.001).minimize(self.cross_entropy)

    def start_session(self):
        init = tf.initialize_all_variables()
        self.session.run(init)
        self.session_started = True

    def train(self, input_data, iteration_num):
        assert len(input_data.shape) == 2

        if not self.session_started:
            self.start_session()

        for i in range(iteration_num):
            indices = range(0, input_data.shape[0])
            np.random.shuffle(indices)
            indices = indices[:BATCH_SIZE]

            batch_x = input_data[indices]
            batch_fd = {self.x: batch_x}
            self.session.run(self.train_step, feed_dict=batch_fd)

            if i % 1000 == 0:
                print "Autoencoder, iteration: ", i
                print "Cross entropy: ", self.session.run(self.cross_entropy, feed_dict=batch_fd)

    def get_w_1(self):
        assert self.session_started

        return self.session.run(self.w_1)

    def get_b_1(self):
        assert self.session_started

        return self.session.run(self.b_1)

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_value, traceback):
        self.session.close()

import numpy as np
import matplotlib
import operator

from dynamic_shape_tuning import convert_mask_to_contour
from matplotlib import pyplot as plt
from obtain_lmdb_dataset import load
from stacked_ae import *

POINT_NUM = 16
SET = 'val'

r_arr, r_l_arr = load(SET + '_images_lmdb', SET + '_labels_lmdb')
r_arr = r_arr / r_arr.max()

if SET == 'val':
    rois, _, coords = obtain_rois(r_arr, r_l_arr, filter_size=(11, 11), filter_num=100)

    rois = np.array([imresize(img, [DOWNSAMPLED_IMAGE_SIZE, DOWNSAMPLED_IMAGE_SIZE])
                     for img in rois], dtype=np.float32)

    tf.reset_default_graph()
    rois_l = predict(rois, 200)
    rois_l = np.reshape(rois_l, [-1, DOWNSAMPLED_IMAGE_SIZE, DOWNSAMPLED_IMAGE_SIZE]).astype(np.float32)
    rois_l = np.array([imresize(img, [ROI_SIZE, ROI_SIZE], mode='F') for img in rois_l], dtype=np.float32)
    # r_arr = np.array([imresize(img, [256, 256]) for img in r_arr], dtype=np.float32)
    # r_l_arr = np.array([imresize(img, [256, 256]) for img in r_l_arr], dtype=np.float32)
    rois_l = rois_l >= THRESHOLD

elif SET == 'train':
    coords = []
    rois_l = []

    for r, r_l in zip(r, r_l_arr):
        contour = convert_mask_to_contour(r_l)
        contour = np.where(contour)
        center_x = np.mean(contour[1])
        center_y = np.mean(contour[0])
        coords.append((center_x - ROI_SIZE / 2, center_y - ROI_SIZE / 2,
                       center_x + ROI_SIZE / 2, center_y + ROI_SIZE / 2))

        rois_l.append(r[center_y - ROI_SIZE / 2:center_y + ROI_SIZE / 2 + 1]
                      [center_x - ROI_SIZE / 2:center_x + ROI_SIZE / 2 + 1])
else:
    assert False

counter = 0
with open(SET + '_images/mri_images.list', 'w') as f:
    pass
for r, r_l, roi_l, coord in zip(r_arr, r_l_arr, rois_l, coords):
    matplotlib.image.imsave(SET + '_images/im' + str(counter) + '.png', r)

    mask = np.zeros_like(r)
    mask[coord[1]:coord[3]+1, coord[0]:coord[2]+1] = roi_l
    contour = np.where(convert_mask_to_contour(mask))

    center_x = np.where(mask)[1].mean()
    center_y = np.where(mask)[0].mean()

    # print contour
    result = []
    result_pred = []

    for i in range(POINT_NUM):
        angle = 2 * float(i) / POINT_NUM * np.pi

        # tmp_result_pos = []
        # tmp_result_neg = []

        tmp_result = []
        for x, y in zip(contour[1], contour[0]):
            x -= center_x
            y -= center_y
            # plt.scatter(x, y, marker='o')
            dist = np.linalg.norm([x, y])
            # xd = np.abs(dist * np.cos(angle) - x) if x >= 0 else np.abs(-dist * np.cos(angle) - x)
            # yd = np.abs(dist * np.sin(angle) - y) if x >= 0 else np.abs(-dist * np.sin(angle) - y)

            xd = np.abs(dist * np.cos(angle) - x)
            yd = np.abs(dist * np.sin(angle) - y)

            if xd < 1 and yd < 1:
                # if x >= 0:
                #     tmp_result_pos.append((x, y))
                # else:
                #     tmp_result_neg.append((x, y))
                tmp_result.append((x, y))

        assert len(tmp_result) > 0
        for t in tmp_result:
            t = (t[0] + center_y, t[1] + center_x)

        if len(tmp_result) == 1:
            result.extend(tmp_result)
        else:
            result.append(tuple([int(np.round(float(value) / len(tmp_result))) for value in
                                 reduce(lambda (a, b), (c, d): (a + c, b + d), tmp_result)]))

    with open(SET + '_images/mri_images.list', 'a') as fl:
        fl.write(SET + '_images/im' + str(counter) + '.pts\n')

    if SET == 'val':
        with open(SET + '_images/' + 'im' + str(counter) + '.pred.pts', 'w') as f:
            f.write('version: 1\n')
            f.write('n_points: ' + str(len(result)) + "\n{\n")
            for res in result:
                f.write(str(res[1]) + " " + str(res[0]) + "\n")
            f.write('}')

    with open(SET + '_images/' + 'im' + str(counter) + '.pts', 'w') as f:
        f.write('version: 1\n')
        f.write('n_points: ' + str(len(result)) + "\n{\n")
        for res in result:
            f.write(str(res[1]) + " " + str(res[0]) + "\n")
        f.write('}')

    counter += 1

from conv_nn_roi import *

IMAGE_SIZE = 256
DOWNSAMPLED_IMAGE_SIZE = 64
MASK_IMAGE_SIZE = 32
ROI_SIZE = 101


def obtain_rois(images, labels_ref, filter_size, filter_num):
    assert 0 < len(filter_size) < 3

    filter_w = filter_size if len(filter_size) == 1 else filter_size[1]
    filter_h = filter_size if len(filter_size) == 1 else filter_size[0]

    images_ref = np.copy(images)
    images = np.array([imresize(img, (DOWNSAMPLED_IMAGE_SIZE, DOWNSAMPLED_IMAGE_SIZE)) for img in images])
    images = images.astype(np.float32)
    result = np.empty([images.shape[0], ROI_SIZE, ROI_SIZE])
    result_labels = np.empty([images.shape[0], ROI_SIZE, ROI_SIZE])
    result_coords = []
    W_conv1 = weight_variable([filter_h, filter_w, 1, filter_num], name="W_conv1")
    b_conv1 = bias_variable([filter_num], name="b_conv1")

    x = tf.placeholder(tf.float32, shape=[None, np.square(DOWNSAMPLED_IMAGE_SIZE)])
    x_image = tf.reshape(x, [-1, DOWNSAMPLED_IMAGE_SIZE, DOWNSAMPLED_IMAGE_SIZE, 1])
    h_conv1 = tf.nn.sigmoid(conv2d(x_image, W_conv1) + b_conv1)

    assert h_conv1.get_shape()[1] == DOWNSAMPLED_IMAGE_SIZE - 2 * (int(filter_h) / 2)
    assert h_conv1.get_shape()[2] == DOWNSAMPLED_IMAGE_SIZE - 2 * (int(filter_w) / 2)

    h_pool1 = avg_pool_6x6(h_conv1)

    pooled_w = int(h_conv1.get_shape()[2]) / 6
    pooled_h = int(h_conv1.get_shape()[1]) / 6
    assert h_pool1.get_shape()[1] == pooled_h
    assert h_pool1.get_shape()[2] == pooled_w

    W_fc1 = weight_variable([pooled_h * pooled_w * filter_num, np.square(MASK_IMAGE_SIZE)], "W_fc1")
    b_fc1 = bias_variable([np.square(MASK_IMAGE_SIZE)], "b_fc1")

    h_pool1_flat = tf.reshape(h_pool1, [-1, pooled_w * pooled_h * filter_num])
    h_fc1 = tf.sigmoid(tf.matmul(h_pool1_flat, W_fc1) + b_fc1)

    # Add ops to save and restore all the variables.
    saver = tf.train.Saver()

    # model_files = [os.path.join(dirpath, f)
    #                for dirpath, _, files in os.walk('./')
    #                for f in fnmatch.filter(files, '*.ckpt')]

    # TODO select the model you need
    # model_file = model_files[0]
    with tf.Session() as session:
        saver.restore(session, './model2016-03-20 21:08.ckpt')
        print("Model restored.")

        result_counter = 0
        for i in range(images.shape[0] / BATCH_SIZE + (images.shape[0] % BATCH_SIZE != 0)):
            batch_x = np.reshape(images[i * BATCH_SIZE: (i + 1) * BATCH_SIZE],
                                 [-1, images.shape[1] * images.shape[2]])
            output = h_fc1.eval(feed_dict={x: batch_x})
            output = np.reshape(output, [output.shape[0], MASK_IMAGE_SIZE, MASK_IMAGE_SIZE])
            coords = [ndimage.measurements.center_of_mass(c >= 0.5) for c in output]
            x1s = [np.round(c[1] * IMAGE_SIZE / MASK_IMAGE_SIZE) - ROI_SIZE / 2 for c in coords]
            x2s = [np.round(c[1] * IMAGE_SIZE / MASK_IMAGE_SIZE) + ROI_SIZE / 2 for c in coords]
            y1s = [np.round(c[0] * IMAGE_SIZE / MASK_IMAGE_SIZE) - ROI_SIZE / 2 for c in coords]
            y2s = [np.round(c[0] * IMAGE_SIZE / MASK_IMAGE_SIZE) + ROI_SIZE / 2 for c in coords]

            # plt.figure(i)
            c = 1
            for x1, x2, y1, y2 in zip(x1s, x2s, y1s, y2s):
                result[result_counter] = np.copy(images_ref[result_counter][y1:y2 + 1, x1:x2 + 1])
                result_labels[result_counter] = np.copy(labels_ref[result_counter][y1:y2 + 1, x1:x2 + 1])
                result_coords.append((int(x1), int(y1), int(x2), int(y2)))
                leftmost_ref = np.argwhere(np.sum(labels_ref[result_counter], 0) >= 1)[0]
                rightmost_ref = labels_ref.shape[2] - 1 - \
                                np.argwhere(np.sum(labels_ref[result_counter], 0)[::-1] >= 1)[0]

                topmost_ref = np.argwhere(np.sum(labels_ref[result_counter], 1) >= 1)[0]
                bottommost_ref = labels_ref.shape[1] - 1 - \
                                 np.argwhere(np.sum(labels_ref[result_counter], 1)[::-1] >= 1)[0]

                if x1 > leftmost_ref or x2 < rightmost_ref or y1 > topmost_ref or y2 < bottommost_ref:
                    print "WARNING, ROI %d doesn't contain the entire heart" % result_counter

                # plt.subplot(5, 8, c)
                # plt.imshow(result[result_counter])
                # plt.subplot(5, 8, c + 1)
                # plt.imshow(result_labels[result_counter])

                c += 2
                result_counter += 1

        plt.show(block=True)

    return result, result_labels, result_coords


if __name__ == "__main__":
    images, labels_ref = obtain_lmdb_dataset.load('./val_images_lmdb', './val_labels_lmdb')
    r, r_l, coords = obtain_rois(images, labels_ref, filter_size=(11, 11), filter_num=100)
    with open("./rois_val" + datetime.datetime.now().strftime("%Y-%m-%d %H:%M.npz"), "w") as f:
        np.savez_compressed(f, data=r)

    with open("./rois_val_labels" + datetime.datetime.now().strftime("%Y-%m-%d %H:%M.npz"), "w") as f:
        np.savez_compressed(f, data=r_l)

    with open("./rois_coords" + datetime.datetime.now().strftime("%Y-%m-%d %H:%M.npz"), "w") as f:
        np.savez_compressed(f, data=coords)

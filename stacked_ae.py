from pretrain_filters import *
from obtain_rois import *
from scipy.misc import imresize
from matplotlib import pyplot as plt

import datetime

LAMBDA = 0.00003
BETA = 3
THRESHOLD = 0.5
P = 0.1
DOWNSAMPLED_IMAGE_SIZE = 64
BATCH_SIZE = 20
ITERATION_NUM = 20000


def initialize_new_variables(session):
    uninitialized_vars = []
    for var in tf.all_variables():
        try:
            session.run(var)
        except tf.errors.FailedPreconditionError:
            uninitialized_vars.append(var)

    init_new_vars_op = tf.initialize_variables(uninitialized_vars)
    session.run(init_new_vars_op)


def init_session():
    model = tf.initialize_all_variables()
    config = tf.ConfigProto()
    config.gpu_options.allocator_type = 'BFC'
    session = tf.Session(config=config)
    session.run(model)

    return session


def train(session, train_step, train_step_num, feed_dict, feed_dict_decipher, y, cross_entropy):
    assert feed_dict_decipher['x'] is not None
    assert feed_dict_decipher['y_'] is not None

    y_ = feed_dict[feed_dict_decipher['y_']]
    x = np.copy(feed_dict[feed_dict_decipher['x']])

    assert len(y_.shape) == 2
    assert len(x.shape) == 2

    for i in range(train_step_num):
        indices = range(0, x.shape[0])
        np.random.shuffle(indices)
        indices = indices[:BATCH_SIZE]

        batch_x = x[indices]
        batch_y = y_[indices]
        batch_fd = {feed_dict_decipher['x']: batch_x, feed_dict_decipher['y_']: batch_y}

        session.run(train_step, feed_dict=batch_fd)

        if i % 10000 == 9999:
            print "Cross entropy: ", session.run(cross_entropy, feed_dict=batch_fd)
            print "MSE: ", session.run(tf.reduce_mean(tf.square(y - batch_y)), feed_dict=batch_fd)

    #         plt.figure(i * 10 + 1)
    #         plt.imshow(np.reshape(y_[0], [DOWNSAMPLED_IMAGE_SIZE, DOWNSAMPLED_IMAGE_SIZE]))
    #         plt.figure(i * 10 + 2)
    #         plt.imshow(np.reshape(session.run(y, feed_dict=batch_fd)[0],
    #                               [DOWNSAMPLED_IMAGE_SIZE, DOWNSAMPLED_IMAGE_SIZE]))
    #
    # plt.show(block=True)


def train_stacked_ae(rois, labels, hidden_layer_size):
    # Rois is a 3d np array with images, intensities must be in [0 .. 1]
    assert len(rois.shape) == 3
    assert labels.shape == rois.shape

    roi_num = rois.shape[0]
    height = rois.shape[1]
    width = rois.shape[2]

    rois_r = np.reshape(rois, [rois.shape[0], height * width])
    labels_r = np.reshape(labels, [labels.shape[0], height * width])

    # First we need to train W4 in non-supervised manner
    W_4 = weight_variable([height * width, hidden_layer_size], "W_4_1")
    b_4 = bias_variable([hidden_layer_size], "b_4_1")

    x = tf.placeholder(tf.float32, shape=[None, height * width])
    y_ = tf.placeholder(tf.float32, shape=[None, height * width])

    h_4 = tf.nn.sigmoid(tf.matmul(x, W_4) + b_4)

    W_5 = weight_variable([hidden_layer_size, height * width], "W_5_1")
    b_5 = bias_variable([height * width], "b_5_1")

    y = tf.nn.sigmoid(tf.matmul(h_4, W_5) + b_5)

    p_cap = tf.reduce_mean(h_4, 0)
    cross_entropy = tf.reduce_sum(tf.square(y - y_)) + \
        LAMBDA / 2 * tf.reduce_sum(tf.square(W_4)) + tf.reduce_sum(tf.square(W_5)) + \
        BETA * tf.reduce_sum(P * tf.log(P / p_cap) + (1 - P) * tf.log((1 - P) / (1 - p_cap)))

    train_step = tf.train.AdamOptimizer(1e-4).minimize(cross_entropy)
    session = init_session()

    feed_dict = {x: rois_r, y_: rois_r}
    feed_dict_decipher = {'x': x, 'y_': y_}
    train(session, train_step, ITERATION_NUM, feed_dict, feed_dict_decipher, y, cross_entropy)

    # W_5 training
    W_5 = weight_variable([hidden_layer_size, hidden_layer_size], "W_5_2")
    b_5 = bias_variable([hidden_layer_size], "b_5_2")

    h_5 = tf.sigmoid(tf.matmul(h_4, W_5) + b_5)

    W_6 = weight_variable([hidden_layer_size, height * width], "W_6_2")
    b_6 = bias_variable([height * width], "b_6_2")

    y = tf.nn.sigmoid(tf.matmul(h_5, W_6) + b_6)

    p_cap = tf.reduce_mean(h_5, 0)
    cross_entropy = tf.reduce_sum(tf.square(y - y_)) + \
        LAMBDA / 2 * tf.reduce_sum(tf.square(W_4)) + tf.reduce_sum(tf.square(W_5)) + tf.reduce_sum(tf.square(W_6)) + \
        BETA * tf.reduce_sum(P * tf.log(P / p_cap) + (1 - P) * tf.log((1 - P) / (1 - p_cap)))

    train_step = tf.train.AdamOptimizer(1e-4).minimize(cross_entropy)

    initialize_new_variables(session)
    train(session, train_step, ITERATION_NUM, feed_dict, feed_dict_decipher, y, cross_entropy)

    # Final pre-training
    W_6 = weight_variable([hidden_layer_size, height * width], "W_6_3")
    b_6 = bias_variable([height * width], "b_6_3")

    y = tf.nn.sigmoid(tf.matmul(h_5, W_6) + b_6)

    cross_entropy = tf.reduce_sum(tf.square(y - y_)) + \
        LAMBDA / 2 * tf.reduce_sum(tf.square(W_6))

    train_step = tf.train.AdamOptimizer(1e-5).minimize(cross_entropy)

    initialize_new_variables(session)
    feed_dict[y_] = labels_r
    train(session, train_step, ITERATION_NUM, feed_dict, feed_dict_decipher, y, cross_entropy)

    # Final fine-tuning
    cross_entropy = tf.reduce_sum(tf.square(y - y_)) + \
        LAMBDA / 2 * tf.reduce_sum(tf.square(W_4)) + tf.reduce_sum(tf.square(W_5)) + tf.reduce_sum(tf.square(W_6))

    train_step = tf.train.AdamOptimizer(1e-5).minimize(cross_entropy)

    saver = tf.train.Saver()
    initialize_new_variables(session)
    train(session, train_step, ITERATION_NUM, feed_dict, feed_dict_decipher, y, cross_entropy)
    save_path = saver.save(session, "./stacked_ae_model" +
                           datetime.datetime.now().strftime("%Y-%m-%d %H:%M") + ".ckpt")
    print("Model saved in file: %s" % save_path)


def predict(rois, hidden_layer_size):
    roi_num = rois.shape[0]
    height = rois.shape[1]
    width = rois.shape[2]
    rois_r = np.reshape(rois, [rois.shape[0], height * width]) / rois.max()

    W_4 = weight_variable([height * width, hidden_layer_size], "W_4_1")
    b_4 = bias_variable([hidden_layer_size], "b_4_1")

    x = tf.placeholder(tf.float32, shape=[None, height * width])
    y_ = tf.placeholder(tf.float32, shape=[None, height * width])

    h_4 = tf.nn.sigmoid(tf.matmul(x, W_4) + b_4)

    W_5 = weight_variable([hidden_layer_size, hidden_layer_size], "W_5_2")
    b_5 = bias_variable([hidden_layer_size], "b_5_2")

    h_5 = tf.sigmoid(tf.matmul(h_4, W_5) + b_5)

    W_6 = weight_variable([hidden_layer_size, height * width], "W_6_3")
    b_6 = bias_variable([height * width], "b_6_3")

    y = tf.nn.sigmoid(tf.matmul(h_5, W_6) + b_6)

    saver = tf.train.Saver()
    session = init_session()
    saver.restore(session, "./stacked_ae_model2016-03-20 21:37.ckpt")

    return session.run(y, feed_dict={x: rois_r})

if __name__ == "__main__":
    # r = np.load('./rois_val2016-03-20 21:51.npz')['data']
    # r_l = np.load('./rois_val_labels2016-03-20 21:51.npz')['data']

    images, labels_ref = obtain_lmdb_dataset.load('./val_images_lmdb', './val_labels_lmdb')
    r, r_l, coords = obtain_rois(images, labels_ref, filter_size=(11, 11), filter_num=100)
    tf.reset_default_graph()

    r = np.array([imresize(img, [DOWNSAMPLED_IMAGE_SIZE, DOWNSAMPLED_IMAGE_SIZE]) for img in r], dtype=np.float32)
    r_l = np.array([imresize(img, [DOWNSAMPLED_IMAGE_SIZE, DOWNSAMPLED_IMAGE_SIZE]) for img in r_l], dtype=np.float32)
    r = r / r.max()
    r_l = r_l / r_l.max()
    # train_stacked_ae(r, r_l, 200)
    p = predict(r, 200)

    print 123
